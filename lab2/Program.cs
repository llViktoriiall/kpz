using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
namespace KPZ__2
{
    public class Book
    {
        public string bookAuthor;
        public string bookName;
        public Book(string author, string name)
        {
            bookAuthor = author;
            bookName = name;
        }
    }
    public class BookShop
    {
        string path = @"e:\BookShopLog.txt";
        private FileStream fs = null;
        public void createLog()
        {
            try
            {
                fs = File.Create(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public void closeLog()
        {
            fs.Close();
        }
        public delegate void EventHandler(string message, Book book);
        public event EventHandler BookOperation;
        public delegate void CEventHandler(string message, int num);
        public event CEventHandler CashOperation;
        private List<Book> Books = new List<Book>();
        private int Cash = 0;
        public void Put(Book book)
        {
            Books.Add(book);
            if (BookOperation == null)
            {
                byte[] info = new UTF8Encoding(true).GetBytes($"There is a new book {book.bookAuthor} {book.bookName}\n");
                fs.Write(info, 0, info.Length);
                Console.WriteLine("New info in the log");
            }
            else
            {
                BookOperation?.Invoke($"There is a new book ", book);
            }
        }
        public void Sell(Book book, int sum)
        {
            if (Books.Contains(book))
            {
                Books.Remove(book);
                Cash += sum;
                if (BookOperation == null)
                {
                    byte[] info = new UTF8Encoding(true).GetBytes($"Book was bought: {book.bookAuthor} {book.bookName}\n");
                    fs.Write(info, 0, info.Length);
                    Console.WriteLine("New info in the log");
                }
                else
                {
                    BookOperation.Invoke($"Book was bought", book);
                }
                if (CashOperation == null)
                {
                    byte[] info = new UTF8Encoding(true).GetBytes($"{sum} added. Current sum: {Cash}\n");
                    fs.Write(info, 0, info.Length);
                    Console.WriteLine("New info in the log");
                }
                else
                {
                    CashOperation?.Invoke($"Cash added {sum}", Cash);
                }
            }
            else
            {
                if (CashOperation == null)
                {
                    byte[] info = new UTF8Encoding(true).GetBytes($"We don't have this book {book.bookAuthor} {book.bookName}\n");
                    fs.Write(info, 0, info.Length);
                    Console.WriteLine("New info in the log");
                }
                else
                {
                    BookOperation?.Invoke($"\n\nWe don't have this book", book); ;
                }
            }

        }

        public void showInfo()
        {
            Console.WriteLine("Books in the shop:");
            for (int i = 0; i < Books.Count; ++i)
            {
                Console.WriteLine(Books[i].bookName);
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Book b1 = new Book("Harlan Ellison", "\"Approaching oblivion\"");
            Book b2 = new Book("Howard Lovecraft", "\"Beyond the Wall of the Sleep\"");
            Book b3 = new Book("Aghata Christie", "\"Miss Marple\"");
            string answer;
            BookShop bookShop = new BookShop();
            Console.WriteLine("Would you like to see all the info in the console?(yes/no)");
            answer = Console.ReadLine();
            if (answer == "yes")
            {
                bookShop.BookOperation += DisplayMessageBook;
                bookShop.CashOperation += DisplayMessageCash;
            }
            if (answer == "no")
            {
                bookShop.createLog();
            }
            bookShop.Put(b1);
            bookShop.Put(b2);
            bookShop.Put(b3);
            Console.WriteLine("\n\n---------------Books in the shop---------------");
            bookShop.showInfo(); 
            bookShop.Sell(new Book("Stephen King", "\"The Tower\""), 25);
            bookShop.Sell(b2, 25);
            bookShop.Sell(b3, 45);
            Console.WriteLine("\n\n---------------Books in the shop---------------");
            bookShop.showInfo();
            if (answer == "no")
            {
                bookShop.closeLog();
            }
            Console.Read();
        }
        private static void DisplayMessageCash(string message, int sum)
        {
            Console.WriteLine(message); 
            Console.WriteLine($"Total sum: {sum}\n\n");
        }
        private static void DisplayMessageBook(string message, Book book)
        {
            Console.WriteLine(message);
            Console.WriteLine($"Name: {book.bookName}");
            Console.WriteLine($"Author: {book.bookAuthor}\n\n");
        }
    }
}
