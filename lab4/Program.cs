﻿using System;
using System.Collections.Generic;

namespace KPZ_4
{
    class Program
    {
        static void Main(string[] args)
        { 
            Person p1 = new Guest();
            p1.Name = "Mary";
            p1.Surname = "McBrown";
            p1.showInfo();
            GuestRoom ec1 = new EconomRoom(15); 
            ec1.setGuest(p1);
            ec1.roomNumber = 25;
            ec1.floor = 2;
            ec1.ShowMainInfo();
            p1.changeRoom(ref ec1.roomNumber, 24);
            ec1.ShowMainInfo();
            ec1.fridgeOperations(GuestRoom.fridgeSnacks.Candies);
            int sum;
            int total;
            ec1.countSum(5, out sum, out total);
            object money = total;
            Console.WriteLine($"Guest's payment for room is {sum} and full payment is {money}");
            money = 100;
            total = (int)money;
            Console.WriteLine($"Guest's payment for room is {sum} and full payment is {total}");
            Meals m = new Meals();
            m.mealInfo(Meals.breakfasts.Oates, Meals.dinners.Salad);
        }
    }

    public interface Econom
    {
        public string Status()
        {
            return "This is econom class room.";
        }
    }
    public interface Standart
    {
        public string Status()
        {
            return "This is standart class room.";
        }
    }
    public interface Luxury
    {
        public string Status()
        {
            return "This is luxury class room.";
        }
    }
    public interface Room
    {
        void Clean()
        {
            Console.WriteLine("This room was cleaned.");
        }
    }
    public abstract class GuestRoom: Room
    {
        public enum fridgeSnacks
        {
            Chocolate,
            Chips,
            Sandwich,
            Candies,
            Jello
        }
        protected bool isCleanedToday;
        public int roomNumber;
        public int floor;
        private int pricePerDay;
        int fridgeSum = 0;   
        List<string> Fridge = new List<string>();
        Person Guest;
        public void setGuest(Person p)
        {
            Guest = p;
        }
        public void setPrice(int price)
        {
            pricePerDay = price;
        }
        public int getPrice()
        {
            return pricePerDay;
        }
        public void fridgeOperations(fridgeSnacks snack)
        { 
            if(snack == fridgeSnacks.Candies || snack == fridgeSnacks.Jello)
            {
                fridgeSum += 1;
            }
            if (snack == fridgeSnacks.Chocolate && snack == fridgeSnacks.Chips)
            {
                fridgeSum += 2;
            }
            if (snack == fridgeSnacks.Sandwich)
            {
                fridgeSum += 3;
            }
        }
        
        public void countSum(int days, out int roomCount, out int totalCount)
        {
            roomCount = pricePerDay * days;
            totalCount = roomCount + fridgeSum;
        }

        public virtual void ShowMainInfo()
        {
            Console.WriteLine($"Room: {roomNumber}\tFloor: {floor}");
        }

    }

    public class EconomRoom : GuestRoom, Econom
    {
        public EconomRoom(int price)
        {
            this.setPrice(price);
        }
        public override void ShowMainInfo()
        {
            base.ShowMainInfo();
            Console.WriteLine("In this room you can call and use TV");
        }
    }
    public class StandartRoom : GuestRoom, Standart
    {
        public override void ShowMainInfo()
        {
            base.ShowMainInfo();
            Console.WriteLine("In this room you can call, use TV, you have fridge and better view");
        }
    }
    public class LuxuryRoom : GuestRoom, Luxury
    {
        public override void ShowMainInfo()
        {
            base.ShowMainInfo();
            Console.WriteLine("In this room you can call, use TV, use fridge, have extra room, have balcony, meal is included in price");
        }
    }

    public class Meals
    {
        public enum breakfasts
        {
            English_breakfast,
            Pancakes,
            Fruit_salad,
            Oates,
            Toasts
        }

        public enum dinners
        {
            Roasted_beef,
            Fried_chicken,
            Salad,
            Baked_fish,
            Mashed_potatoes
        }

        private class Menu
        {
            breakfasts breakfast;
            dinners dinner;
            public Menu(breakfasts b, dinners d)
            {
                breakfast = b;
                dinner = d;
            }
            public breakfasts getBreakfast()
            {
                return breakfast;
            }
            public dinners getDinner()
            {
                return dinner;
            }
        }
        
        public void mealInfo(breakfasts b, dinners d)
        {
            Menu m = new Menu(b, d);
            Console.WriteLine($"Todas menu is: {m.getBreakfast()} and {m.getDinner()} ");
        }
    }

    public abstract class Person
    {
        public string Name;
        public string Surname;        
        public virtual void showInfo()
        {
            Console.WriteLine($"{Name} {Surname}");
        }
        public void changeRoom(ref int roomNumber, int newRoomMumber)
        {
            roomNumber = newRoomMumber;
        }
    }

    public class Guest: Person
    {
        static Guest()
        {
            Console.WriteLine("Object created");
        }
        static string status = "Guest";
        public override void showInfo()
        {
            base.showInfo();
            Console.WriteLine($"This is a {status}");
        }
    }

    class Floor
    {
        public int roomAmount { get; set; }

        public static implicit operator Floor(int x)
        {
            return new Floor{ roomAmount = x };
        }
        public static explicit operator int(Floor floor)
        {
            return floor.roomAmount;
        }
    }

}
