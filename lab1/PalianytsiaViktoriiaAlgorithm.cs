using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace PalianytsiaViktoriia.RobotChallange
{
    public class PalianytsiaViktoriiaAlgorithm: IRobotAlgorithm
    {

        public string Author
        {
            get { return "Palianytsia Viktoriia "; }
        }

        public string Description
        {
            get { return "My cool alghorithm "; }
        }

        Position newposition = new Position();

        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }


        public Position FindNearestEnemy(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            Position nearest = null;
            int minDistance = int.MaxValue;
            foreach (var robot in robots)
            {
                int d = DistanceHelper.FindDistance(robot.Position, movingRobot.Position);
                if (d < minDistance && robot.OwnerName != "Palianytsia Viktoriia ")
                {
                    minDistance = d;
                    nearest = robot.Position;
                }
            }
            return nearest == null ? null : nearest;
        }

        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }


        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell && robot.OwnerName == "Palianytsia Viktoriia ")
                        return false;
                }
            }
            return true;
        }

        int diffX = 0;
        int diffY = 0;
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
        int energyCheck = 0;
            Position enemy = new Position();
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            Position stationPosition = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);

            if (movingRobot.Energy > 200 && robots.Count < 1000)
            {
                //counter++;
                ///Position stationPosition = FindNearestFreeStationForRobot(robots[robotToMoveIndex], map, robots, stationPosition);
                return new CreateNewRobotCommand();
            }
            if (stationPosition == movingRobot.Position)
            {
                    return new CollectEnergyCommand();
            }



            if (stationPosition == null)
                return null;
            else
            {
                energyCheck = DistanceHelper.FindDistance(movingRobot.Position, stationPosition);
                if(energyCheck > movingRobot.Energy)
                {
                    if (energyCheck > 100)
                    {
                        enemy = FindNearestEnemy(movingRobot, map, robots);
                        return new MoveCommand() { NewPosition = enemy };
                    }
                    else
                    {
                        diffX = stationPosition.X - movingRobot.Position.X;
                        diffY = stationPosition.Y - movingRobot.Position.Y;
                        Position newPos = new Position();
                        while (energyCheck > movingRobot.Energy)
                        {
                            diffX = diffX / 2;
                            diffY = diffY / 2;
                            newPos.X = movingRobot.Position.X + diffX;
                            newPos.Y = movingRobot.Position.Y + diffY;
                            energyCheck = DistanceHelper.FindDistance(movingRobot.Position, newPos);
                        }
                        return new MoveCommand() { NewPosition = newPos };
                    }
                }
               return new MoveCommand() { NewPosition = stationPosition };              
            }

        }
    }
}
