﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace KPZ__5
{
    [DataContract]
    public class Reservation
    {
        [DataMember]
        public DateTime ArrivalDay { get; set; }
        [DataMember]
        public int DaysAmount { get; set; }
        [DataMember]
        public Status ReservationStatus { get; set; }
    }
    public enum Status
    {
        [EnumMember]
        Booked,
        [EnumMember]
        Canceled,
        [EnumMember]
        Arrived
    }
}
