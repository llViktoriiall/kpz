﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using static KPZ__5.Program;

namespace KPZ__5
{
    [DataContract]
    public class DataModel
    {
        [DataMember]
        public IEnumerable<Room> Rooms { get; set; }
        [DataMember]
        public IEnumerable<Guest> Guests { get; set; }
        [DataMember]
        public IEnumerable<Reservation> Reservations { get; set; }

        public DataModel()
        {
            Rooms = new List<Room>() { new Room() };
            Guests = new List<Guest>() { new Guest() };
            Reservations = new List<Reservation>() { new Reservation()};
        }
        public static string DataPath = "F:/test.dat";
        public static DataModel Load()
        {
            if (File.Exists(DataPath))
            {
                return DataSerializer.DeserializeItem(DataPath);
            }
            return new DataModel();
        }
        public void Save()
        {
            DataSerializer.SerializeData(DataPath, this);
        }
    }
}
