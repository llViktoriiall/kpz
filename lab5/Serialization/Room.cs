﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace KPZ__5
{
    [DataContract]
    public class Room
    {
        [DataMember]
        public int Number { get; set; }
        [DataMember]
        public int Floor { get; set; }
        [DataMember]
        public Class RoomClass { get; set; }
    }
    public enum Class
    {
        [EnumMember]
        Econom,
        [EnumMember]
        Standart,
        [EnumMember]
        Luxury
    }
}
