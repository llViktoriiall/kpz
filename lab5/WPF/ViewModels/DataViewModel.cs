﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using KPZ__5;
using KPZ__5__WPF.ViewModels;

namespace KPZ__5__WPF.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        public DataViewModel()
        {
            SetControlVisibility = new Command(ControlVisibility);
        }

        private string _visibleControl = "Reservations"; 
       
        public string VisibleControl
        {
            get
            {
                return _visibleControl;
            }
            set
            {
                _visibleControl = value;
                OnPropertyChanged("VisibleControl");
            }
        }

        public ICommand SetControlVisibility { get; set; }
        public void ControlVisibility(object args)
        {
            VisibleControl = args.ToString();
        }
        
        private ObservableCollection<RoomViewModel> _Rooms;
        private ObservableCollection<GuestViewModel> _Guests;
        private ObservableCollection<ReservationViewModel> _Reservations;


        public ObservableCollection<RoomViewModel> Rooms
        {
            get
            {
                return _Rooms;
            }
            set
            {
                _Rooms = value;
                OnPropertyChanged("Rooms");
            }
        }

        public ObservableCollection<GuestViewModel> Guests
        {
            get
            {
                return _Guests;
            }
            set
            {
                _Guests = value;
                OnPropertyChanged("Guests");
            }
        }

        public ObservableCollection<ReservationViewModel> Reservations
        {
            get
            {
                return _Reservations;
            }
            set
            {
                _Reservations = value;
                OnPropertyChanged("Reservations");
            }
        }
    }
}
