﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ__5__WPF.ViewModels
{
    public class GuestViewModel : ViewModelBase
    {
        private string _Name;
        private string _Surname;
        public string Name {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
                OnPropertyChanged("Name");
            }
        }
        public string Surname
        {
            get
            {
                return _Surname;
            }
            set
            {
                _Surname = value;
                OnPropertyChanged("Surame");
            }
        }
    }
}
