﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ__5__WPF.ViewModels
{
    public class RoomViewModel : ViewModelBase
    {
        private int _Number;
        private int _Floor;
        private Class _RoomClass;

        public int Number
        {
            get
            {
                return _Number;
            }
            set
            {
                _Number = value;
                OnPropertyChanged("Number");
            }
        }
        public int Floor
        {
            get
            {
                return _Floor;
            }
            set
            {
                _Floor = value;
                OnPropertyChanged("Floor");
            }
        }
        public Class RoomClass
        {
            get
            {
                return _RoomClass;
            }
            set
            {
                _RoomClass = value;
                OnPropertyChanged("RoomClass");
            }
        }
    }
    public enum Class
    {
        Econom,
        Standart,
        Luxury
    }
}
