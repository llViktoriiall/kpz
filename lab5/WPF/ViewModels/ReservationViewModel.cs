﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ__5__WPF.ViewModels
{
    public class ReservationViewModel : ViewModelBase
    {
        private DateTime _ArrivalDay;
        private int _DaysAmount;
        private Status _ReservationStatus;

        public DateTime ArrivalDay {
            get
            {
                return _ArrivalDay;
            }
            set
            {
                _ArrivalDay = value;
                OnPropertyChanged("ArrivalDay");
            }
        }
        public int DaysAmount
        {
            get
            {
                return _DaysAmount;
            }
            set
            {
                _DaysAmount = value;
                OnPropertyChanged("DaysAmount");
            }
        }
        public Status ReservationStatus
        {
            get
            {
                return _ReservationStatus;
            }
            set
            {
                _ReservationStatus = value;
                OnPropertyChanged("ReservationStatus");
            }
        }
    }
    public enum Status
    {
        Booked,
        Canceled,
        Arrived
    }
}
