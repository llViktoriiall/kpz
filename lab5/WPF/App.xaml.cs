﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using KPZ__5;
using KPZ__5__WPF.ViewModels;

namespace KPZ__5__WPF
{
    public partial class App : Application
    {
        private DataModel _model;
        private DataViewModel _viewModel;
        public App()
        {
            _model = DataModel.Load();
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<DataModel, DataViewModel>();
                cfg.CreateMap<DataViewModel, DataModel>();

                cfg.CreateMap<Room, RoomViewModel>();
                cfg.CreateMap<RoomViewModel, Room>();

                cfg.CreateMap<Guest, GuestViewModel>();
                cfg.CreateMap<GuestViewModel, Guest>();

                cfg.CreateMap<Reservation, ReservationViewModel>();
                cfg.CreateMap<ReservationViewModel, Reservation>();
            });
            IMapper iMapper = config.CreateMapper();
            _viewModel = iMapper.Map<DataModel, DataViewModel>(_model);
            var window = new MainWindow() { DataContext = _viewModel };
            window.Show();
        }
        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DataViewModel, DataModel>();
                });
                IMapper iMapper = config.CreateMapper();
                _model = iMapper.Map<DataViewModel, DataModel>(_viewModel);
                _model.Save();
            }
            catch
            {
                base.OnExit(null);
                throw;
            }
        }
    }
}
