﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KPZ_7.Controllers
{
    public class WorkersController : ApiController
    {
        public IEnumerable<Workers> Get()
        {
            using (HotelDB_KPZEntities dbContext = new HotelDB_KPZEntities())
            {
                return dbContext.Workers.ToList();
            }
        }
        public Workers Get(int id)
        {
            using (HotelDB_KPZEntities dbContext = new HotelDB_KPZEntities())
            {
                return dbContext.Workers.FirstOrDefault(e => e.wID == id);
            }
        }
    }
}
