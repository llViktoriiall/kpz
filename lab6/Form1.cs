﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace KPZ6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            string connectionString = @"Data Source=DESKTOP-FN8SKEF;Initial Catalog=HotelDB_KPZ;Integrated Security=True";
            dataGridView1.RowCount = 12;
            dataGridView1.ColumnCount = 6;
            dataGridView4.RowCount = 12;
            dataGridView4.ColumnCount = 6;
            dataGridView1[0, 0].Value = "Number";
            dataGridView1[1, 0].Value = "Floor";
            dataGridView1[2, 0].Value = "Class";
            dataGridView1[3, 0].Value = "Price";
            dataGridView1[4, 0].Value = "Condition";
            dataGridView1[5, 0].Value = "People Amount";
            dataGridView4[0, 0].Value = "Number";
            dataGridView4[1, 0].Value = "Floor";
            dataGridView4[2, 0].Value = "Class";
            dataGridView4[3, 0].Value = "Price";
            dataGridView4[4, 0].Value = "Condition";
            dataGridView4[5, 0].Value = "People Amount";
            using (HotelDB_KPZEntities db = new HotelDB_KPZEntities())
            {
                var rooms = db.Rooms;
                int i = 1;
                foreach (Room r in rooms)
                {
                    dataGridView1[0, i].Value = r.Number;
                    dataGridView1[1, i].Value = r.Floor;
                    dataGridView1[2, i].Value = r.Class;
                    dataGridView1[3, i].Value = r.Price;
                    dataGridView1[4, i].Value = r.Condition;
                    dataGridView1[5, i].Value = r.PeopleAmount;
                    i++;
                }
            }
            dataGridView2.RowCount = 11;
            dataGridView2.ColumnCount = 2;
            dataGridView2[0, 0].Value = "Name";
            dataGridView2[1, 0].Value = "Surname";
            dataGridView5.RowCount = 11;
            dataGridView5.ColumnCount = 2;
            dataGridView5[0, 0].Value = "Name";
            dataGridView5[1, 0].Value = "Surname";
            using (HotelDB_KPZEntities db = new HotelDB_KPZEntities())
            {
                var workers = db.Workers;
                int i = 1;
                foreach (Worker w in workers)
                {
                    dataGridView2[0, i].Value = w.Name;
                    dataGridView2[1, i].Value = w.Surname;
                    i++;
                }
            }
            dataGridView3.RowCount = 11;
            dataGridView3.ColumnCount = 5;
            dataGridView3[0, 0].Value = "Name";
            dataGridView3[1, 0].Value = "Surname";
            dataGridView3[2, 0].Value = "Arrival Date";
            dataGridView3[3, 0].Value = "Eviction Date";
            dataGridView3[4, 0].Value = "Room";
            dataGridView6.RowCount = 11;
            dataGridView6.ColumnCount = 5;
            dataGridView6[0, 0].Value = "Name";
            dataGridView6[1, 0].Value = "Surname";
            dataGridView6[2, 0].Value = "Arrival Date";
            dataGridView6[3, 0].Value = "Eviction Date";
            dataGridView6[4, 0].Value = "Room";
            using (HotelDB_KPZEntities db = new HotelDB_KPZEntities())
            {
                var clients = db.Clients;
                int i = 1;
                foreach (Client c in clients)
                {
                    dataGridView3[0, i].Value = c.Name;
                    dataGridView3[1, i].Value = c.Surname;
                    dataGridView3[2, i].Value = c.ArrivalDate;
                    dataGridView3[3, i].Value = c.EvictionDate;
                    dataGridView3[4, i].Value = c.Room;
                    i++;
                }
            }


            //using (var db = new HotelContext())
            //{
            //    int j = 1;
            //    //db.Workers.Add(new Worker_ { Name = "Anna", Surname = "Tomson" });
            //    foreach (Room_ r in db.Rooms)
            //    {
            //        dataGridView4[0, j].Value = r.Number;
            //        dataGridView4[1, j].Value = r.Floor;
            //        dataGridView4[2, j].Value = r.Class;
            //        dataGridView4[3, j].Value = r.Price;
            //        dataGridView4[4, j].Value = r.Condition;
            //        dataGridView4[5, j].Value = r.PeopleAmount;
            //        j++;
            //    }
            //    j = 1;
            //    foreach (Worker_ w in db.Workers)
            //    {
            //        dataGridView5[0, j].Value = w.Name;
            //        dataGridView5[1, j].Value = w.Surname;
            //        j++;
            //    }
            //    j = 1;
            //    foreach (Client_ c in db.Clients)
            //    {
            //        dataGridView6[0, j].Value = c.Name;
            //        dataGridView6[1, j].Value = c.Surname;
            //        dataGridView6[2, j].Value = c.ArrivalDate;
            //        dataGridView6[3, j].Value = c.EvictionDate;
            //        dataGridView6[4, j].Value = c.Room;
            //        j++;
            //    }
            //}
                
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (HotelDB_KPZEntities db = new HotelDB_KPZEntities())
            {
                Room r = new Room();
                r.Number = Convert.ToInt32(textBox1.Text);
                r.Floor = Convert.ToInt32(textBox2.Text);
                r.Class = textBox3.Text;
                r.Price = Convert.ToInt32(textBox4.Text);
                r.Condition = textBox5.Text;
                r.PeopleAmount = Convert.ToInt32(textBox6.Text);
                db.Rooms.Add(r);
                int i = dataGridView1.RowCount;
                dataGridView1.RowCount++;
                dataGridView1[0, i].Value = r.Number;
                dataGridView1[1, i].Value = r.Floor;
                dataGridView1[2, i].Value = r.Class;
                dataGridView1[3, i].Value = r.Price;
                dataGridView1[4, i].Value = r.Condition;
                dataGridView1[5, i].Value = r.PeopleAmount;
                db.SaveChanges();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int num = Convert.ToInt32(textBox7.Text);
            using (HotelDB_KPZEntities db = new HotelDB_KPZEntities())
            {
                var room = (from r in db.Rooms
                            where r.Number == num
                            select r).SingleOrDefault();
                
                db.Rooms.Remove(room);
                db.SaveChanges();
                dataGridView1.Rows.Clear();
                dataGridView1.RowCount = 11;
                var rooms = db.Rooms;
                int i = 1;
                foreach (Room r in rooms)
                {
                    dataGridView1[0, i].Value = r.Number;
                    dataGridView1[1, i].Value = r.Floor;
                    dataGridView1[2, i].Value = r.Class;
                    dataGridView1[3, i].Value = r.Price;
                    dataGridView1[4, i].Value = r.Condition;
                    dataGridView1[5, i].Value = r.PeopleAmount;
                    i++;
                }
            }
        }

        private void dataGridView5_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
