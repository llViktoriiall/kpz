﻿namespace KPZ6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HotelDBv1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Workers",
                c => new
                    {
                        wID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                    })
                .PrimaryKey(t => t.wID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Workers");
        }
    }
}
