﻿using System;
using System.Linq;
using System.Collections.Generic;
namespace KPZ_3
{
    public class Person
    {
        public Person(string person, string surname, int floor, int room)
        {
            personName = person;
            personSurname = surname;
            floorNumber = floor;
            roomNumber = room;
        }
        public string personName { get; set; }
        public string personSurname { get; set; }
        public int floorNumber { get; set; }
        public int roomNumber { get; set; }

    }

    public class PersonComparer : IComparer<Person>
    {
        public int Compare(Person p1, Person p2)
        {
            if (p1.personSurname[0] > p2.personSurname[0])
                return 1;
            else if (p1.personSurname[0] < p2.personSurname[0])
                return -1;
            else
                return 0;
        }
    }


    public class Floor
    {
        public int floorNumber { get; set; }
        public string floorClass { get;set;}
    }


    public class MockUp
    {

        public List<Floor> Floors
        {
            get
            {
                return new List<Floor>()
                {
                    new Floor(){floorNumber = 1, floorClass = "econom"},
                    new Floor(){floorNumber = 2, floorClass = "standart"},
                    new Floor(){floorNumber = 3, floorClass = "luxury"},
                };
            }
        }

        public List<Person> People
        {
            get
            {
                return new List<Person>()
                {
                      new Person("Steve", "Galagher", 1, 1),
                      new Person("Alexa", "Chang", 1,24),
                      new Person("John","McCall", 1,13),
                      new Person("Mary","Faven", 2,17),
                      new Person("Kevin","Brown", 2,22),
                      new Person("Suzen", "Hellish",2,23),
                      new Person("Carla","Johnson", 3,16),
                      new Person("Mike","Fairwell", 3,23),
                      new Person("Judith", "Oaks", 3,31),
                };
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("\n\tTasks 1,2,5");
            MockUp Info = new MockUp();
            Console.WriteLine("People, who live on the second floor:");
            Info.People.Where(s => s.floorNumber == 2)//task2
                .Select(s => new { s.personName, s.floorNumber, s.personSurname, s.roomNumber })//task1,5
                .OrderBy(s => s.roomNumber)
                .ToList()
                .ForEach(s => Console.WriteLine($"{s.personName} {s.personSurname}\t Room: {s.roomNumber}"));

            Console.WriteLine("\n\tTasks 3,4");

            IEnumerable <Person> en1 =
                from names in Info.People
                where names.floorNumber == 1
                select names;

            IEnumerable<Person> en2 =
                from names in Info.People
                where names.floorNumber == 2
                select names;


            Dictionary<Floor, IEnumerable<Person>> dict = new Dictionary<Floor, IEnumerable<Person>>()//task3
            {
                {Info.Floors[0], en1 },
                {Info.Floors[1], en2 },
                {Info.Floors[2], Info.People.Where(s => s.floorNumber==3)},
            };
            string task = "";
            foreach (var pair in dict)
            {
                Console.WriteLine("Floor: {0} Class: {1}", pair.Key.floorNumber, pair.Key.floorClass);
                task = task.Show(pair.Value);//task4
                Console.WriteLine(task);
            }


            Console.WriteLine("\n\tTasks 6,7");

            Person[] personArray = Info.People.ToArray();//task7
            Array.Sort(personArray, new PersonComparer());//task6,8?
            foreach (Person p in personArray)
            {
                Console.WriteLine($"{p.personSurname} {p.personName}");
            }



            Console.WriteLine("\n\tTask 8");
            Person[] personArray2 = Info.People.ToArray();
            var result = from person in personArray2
                         orderby person.personName
                         select person;
            foreach(Person p in result)
            {
                Console.WriteLine($"{p.personName} {p.personSurname}");
            }

        }
    }


    public static class StringExtension
    {
        public static string Show(this string str, IEnumerable<Person> people)
        {
            string temp = "";
            foreach(var person in people)
            {
                temp += $"|{person.personName} {person.personSurname}|";
            }
            return temp;
        }
    }
}
